#!/bin/bash

set -e

source /etc/environment

# Verify if process is already running
if [ -e /var/run/rabbitmq/pid ]; then
	service rabbitmq-server restart
else
  service rabbitmq-server start
fi

if [ ! -e /docker/rabbitmq/setup/ok ]; then
  if [ `rabbitmqctl list_users | grep $RABBITMQ_DEFAULT_USER | wc -l` = "0" ]; then
    rabbitmqctl add_user $RABBITMQ_DEFAULT_USER $RABBITMQ_DEFAULT_PASS
    rabbitmqctl set_user_tags $RABBITMQ_DEFAULT_USER administrator
    rabbitmqctl set_permissions -p / $RABBITMQ_DEFAULT_USER ".*" ".*" ".*"
  else
    rabbitmqctl change_password $RABBITMQ_DEFAULT_USER $RABBITMQ_DEFAULT_PASS
  fi
  touch /docker/rabbitmq/setup/ok
fi

exit 0
